{
  description = "A CLI for working with the Fedran ecosystem";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";

    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, flake-utils, flake-compat }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        pkgs = import nixpkgs { inherit system; };

        # Information about our project.
        name = "fedran-cli";
        pname = name;
        root = toString ./.;

        ignoreSource = [ ".git" "target" "example" ];

        src = pkgs.nix-gitignore.gitignoreSource ignoreSource root;

        # Pull in the toolchain from the `rustup-toolchain` file to set the
        # version we use.
        RUST_BACKTRACE = 1;
        RUSTUP_TOOLCHAIN = [
          (pkgs.lib.strings.removeSuffix "\n"
            (builtins.readFile ./rustup-toolchain))
        ];

        # We use `rustup` instead of an overlay for the devShell because it
        # makes life a lot easier when working with CLion and VS Code.
        buildInputs = [ pkgs.rustup pkgs.openssl pkgs.pkg-config ];
      in rec {
        # Set up the developer shell which is used by direnv.
        devShell =
          pkgs.mkShell { inherit buildInputs RUST_BACKTRACE RUSTUP_TOOLCHAIN; };
      });
}
