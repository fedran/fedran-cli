#!/usr/bin/env bash

# Download the Fedran covers for inclusion. This places everything into the
# ./build/fedran-covers folder.

# Set up logging.
log() { echo "🖼 $(basename $0): $@"; }

# Move into the root folder.
cd $(dirname $(dirname $0))

# Check to see if it is already downloaded.
if [ -d ./build/fedran-covers ]
then
  log "fedran-covers already exists, not downloading"
else
  # Make sure we have all the required environment variables.
  log "fedran-covers is missing, download from bucket"
  ./scripts/check-env-bucket.sh || exit 1

  # Set the variables.
  export AWS_ACCESS_KEY_ID=$S3_ACCESS_KEY_ID
  export AWS_SECRET_ACCESS_KEY=$S3_SECRET_ACCESS_KEY
  export AWS_ENDPOINT=$S3_ENDPOINT
  export AWS_BUCKET=$S3_BUCKET

  # Download and extract the bucket.
  mkdir -p ./build/fedran-covers
  s3cmd --access_key=$S3_ACCESS_KEY_ID --access_token=$S3_SECRET_ACCESS_KEY --host=$S3_ENDPOINT --host-bucket=$S3_ENDPOINT -P get s3://$S3_BUCKET/fedran-covers.tar.bz2 ./build/fedran-covers/fedran-covers.tar.bz2 || exit 1
  tar x -C build/fedran-covers -f build/fedran-covers/fedran-covers.tar.bz2
fi
