# Tasks

- [x] List covers in the repository
- [x] Add slug to json
- [x] Add cover slug to json
- [x] Figure out if we have a cover or not
- [x] The ability to add languages to the page
- [x] Year of the copyright
- [x] Trigger warnings
- [x] Include metadata from all the chapters
- [x] Split `project sync` into `project update` and `project sync`
- [x] Title page generation
- [x] Source credits
- [ ] `project setup`
    - [x] Generate `publication.json`
        - [x] Insert/remove languages
	- [x] Add in optional covers
	- [x] Create about page
	- [x] Create license page
	- [x] Insert HTML cover
	- [x] Legal page
	- [x] Generate and insert credits
	- [x] Include optional dedication
    - [x] Generate `package.json`
    - [x] Set up `release.config.js`
    - [x] Set up editor config
    - [x] Set up commit lint
    - [ ] Set up Husky
    - [ ] Set up Woodpecker
    - [x] Set up Nix flake
    - [x] Set up mfgames-writing
    - [x] Create HTML covers (need hints on line endings)
    - [x] Create default build scripts/ folder

## General

- [ ] Pick up the copyright year automatically
- [ ] CRUD on languages
- [ ] CRUD on genres
- [ ] CRUD on content warnings (maybe)
- [x] Chapter and total word counts
- [ ] Be able to mark a project as being public
  -  [ ] Make it mandatory for finished ones
- [ ] `project gitea check`
  - [ ] Must be public if the project is finalized
- [ ] `project sync` should chain to `project gitea sync`
- [ ] `project gitea sync`
  - [x] Create project if missing
  - [x] Update project settings if incorrect
  - [ ] Make sure the default branch is protected
  - [x] Set the development URL in the links
  - [x] Investigate `allegro`
  - [x] Investigate `lost-chances`
  - [x] Investigate `figurines`
  - [x] Investigate `songbird-in-the-kitchen`
  - [x] raging-alone

## Identifiers

- [x] List all characters
    - [ ] Search for characters (`characters list [name]`)
- [x] List all volumes
    - [ ] Search for volumes (`volumes list [name]`)
- [ ] Add character
- [ ] Add volume
- [ ] `source guess` needs to handle apostrophes in name
- [ ] Add credits to sources

## Projects

- [x] Project information (`fedran.json`)
- [ ] Linting (`project lint`)
    - [ ] Check for summary and teaser
    - [ ] Look for people, locations, etc
    - [ ] Look for trigger warnings
- [x] Set character (`project character [name]`)
- [x] Set volume (`project volume [name]`)
- [ ] Sync `ProjectStatus` to `VolumeStatus`
    - `project sync`

### States

- [ ] Number of words
- [ ] Genre assigned
- [ ] Content warnings assigned
- [ ] Chapter references documented
- [ ] Chapter timed
- [ ] Description created
- [ ] Credits populated
- [ ] Editors
