use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct ApiUserResponse {
    pub id: u32,
    pub login: String,
    pub email: String,
    pub avatar_url: String,
    pub active: bool,
    pub synced: u64,
}
