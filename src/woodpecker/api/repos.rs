use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct ApiReposResponse {
    pub id: u32,
    pub owner: String,
    pub name: String,
    pub full_name: String,
    pub avatar_url: String,
    pub link_url: String,
    pub clone_url: String,
    pub default_branch: String,
    pub scm: String,
    pub visibility: String,
    pub private: bool,
    pub trusted: bool,
    pub gated: bool,
    pub active: bool,
    pub allow_pr: bool,
    pub config_file: String,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct ApiRepoSetActiveRequest {
    pub active: bool,
    pub allow_pr: bool,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct ApiRepoSecretResponse {
    pub id: u32,
    pub name: String,
    pub event: Vec<String>,
    pub image: Vec<String>,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct ApiRepoSecretCreateRequest {
    pub name: String,
    pub value: String,
    pub event: Vec<String>,
    pub image: Vec<String>,
}
