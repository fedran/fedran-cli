use crate::config::get_config;
use crate::woodpecker::api::repos::{
    ApiRepoSecretCreateRequest, ApiRepoSecretResponse, ApiRepoSetActiveRequest, ApiReposResponse,
};
use crate::woodpecker::api::user::ApiUserResponse;
use anyhow::Result;
use http::header::HeaderMap;
use reqwest::Client;

pub mod api;

pub struct WoodpeckerClient {
    /// Contains the root server including `https://`.
    server_url: String,

    /// Contains the HTTP client for requests.
    client: Client,
}

impl WoodpeckerClient {
    pub fn new() -> Result<WoodpeckerClient> {
        // Get out the configuration.
        let config = get_config()?;

        // Create the headers for each request.
        let mut headers = HeaderMap::new();

        headers.insert(
            "authorization",
            format!("Bearer {}", &config.woodpecker_token.unwrap())
                .parse()
                .unwrap(),
        );

        // Create the basic client.
        let client = reqwest::Client::builder()
            .default_headers(headers)
            .build()?;

        Ok(WoodpeckerClient {
            server_url: "https://ci.mfgames.com".to_string(),
            client,
        })
    }

    /// Constructs a URL from the internal information.
    pub fn get_url(&self, relative: &str) -> String {
        format!("{}/api/{}", self.server_url, relative)
    }

    /// Retrieves information about the current user.
    pub async fn get_user(&self) -> Result<ApiUserResponse> {
        let url = self.get_url("user");
        log::debug!("retrieving {}", url);
        Ok(self.client.get(url).send().await?.json().await?)
    }

    /// Retrieves information about a specific repository.
    pub async fn get_repo(&self, owner: &str, repo: &str) -> Result<ApiReposResponse> {
        let url = self.get_url(&format!("repos/{}/{}", owner, repo));
        Ok(self.client.get(url).send().await?.json().await?)
    }

    pub async fn set_repo_active(
        &self,
        owner: &str,
        repo: &str,
        active: bool,
        allow_pr: bool,
    ) -> Result<ApiReposResponse> {
        let url = self.get_url(&format!("repos/{}/{}", owner, repo));
        let req = ApiRepoSetActiveRequest { active, allow_pr };
        Ok(self
            .client
            .post(url)
            .json(&req)
            .send()
            .await?
            .json()
            .await?)
    }

    pub async fn set_repo_secret(
        &self,
        owner: &str,
        repo: &str,
        name: &str,
        value: &str,
    ) -> Result<ApiRepoSecretResponse> {
        let delete_url = self.get_url(&format!("repos/{}/{}/secrets/{}", owner, repo, name));
        let post_url = self.get_url(&format!("repos/{}/{}/secrets", owner, repo));

        let req = ApiRepoSecretCreateRequest {
            name: name.to_string(),
            value: value.to_string(),
            event: vec![
                "push".to_string(),
                "tag".to_string(),
                "deployment".to_string(),
            ],
            image: vec![],
        };

        log::debug!("deleting {}", delete_url);
        let _ = self.client.delete(&delete_url).send().await;

        log::debug!("creating {}", delete_url);
        Ok(self
            .client
            .post(&post_url)
            .json(&req)
            .send()
            .await?
            .json()
            .await?)
    }
}
