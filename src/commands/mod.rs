use anyhow::Result;
use clap::{Parser, Subcommand};
use simple_logger::SimpleLogger;

mod config;
mod data;
mod project;

/// The root command for the entire application, this includes all the top-level
/// sub-commands, common options for logging, and other related elements.
#[derive(Parser, Debug)]
#[clap(
    name = "fedran-cli",
    about = "A CLI tool for working with the Fedran ecosystem"
)]
pub struct RootCommand {
    /// Increases the verbosity of output by showing increasing levels of
    /// detail logging.
    #[arg(short, long, action = clap::ArgAction::Count, global = true)]
    pub verbose: u8,

    #[command(subcommand)]
    pub cmd: RootCommands,
}

#[derive(Subcommand, Debug)]
pub enum RootCommands {
    #[command(name = "config", about = "Configure the environment")]
    Config(config::ConfigCommand),

    #[command(name = "data", about = "Query and manipulate shared metadata")]
    Data(data::DataCommand),

    #[command(name = "project", about = "Works with the project")]
    Project(project::ProjectCommand),
}

impl RootCommand {
    pub async fn run(&self) -> Result<()> {
        self.setup_logging();

        let result = match &self.cmd {
            RootCommands::Config(cmd) => cmd.run().await,
            RootCommands::Data(cmd) => cmd.run().await,
            RootCommands::Project(cmd) => cmd.run().await,
        };
        result
    }

    pub fn setup_logging(&self) {
        let level = match self.verbose {
            0 => log::LevelFilter::Warn,
            1 => log::LevelFilter::Info,
            2 => log::LevelFilter::Debug,
            _ => log::LevelFilter::Trace,
        };

        SimpleLogger::new()
            .with_level(level)
            .with_utc_timestamps()
            .env()
            .init()
            .unwrap();
    }
}
