use crate::data::sources::SourceData;
use anyhow::Result;
use clap::Parser;
use prettytable::*;

/// Lists all the volumes known to the identifiers.
#[derive(Parser, Debug)]
pub struct SourcesCommand {}

impl SourcesCommand {
    /// Executes the configuration sub-command.
    pub async fn run(&self) -> Result<()> {
        // Get the sources from a local drive or remotely.
        let sources = SourceData::get().await?;

        // Set up the table and formatting.
        let mut table = Table::new();
        table.set_format(*format::consts::FORMAT_NO_BORDER_LINE_SEPARATOR);
        table.set_titles(row!["identifier", "character", "title", "slug"]);

        // Go through and display each source.
        for source in &sources.sources {
            let source = source.borrow();
            let character_name = sources
                .characters
                .iter()
                .filter(|x| x.borrow().pov == source.pov)
                .map(|x| x.borrow().name.as_str().to_string())
                .collect::<String>();

            table.add_row(row![
                source.format_identifier(),
                character_name,
                source.title,
                source.get_slug()
            ]);
        }

        // Write out the table.
        table.printstd();
        Ok(())
    }
}
