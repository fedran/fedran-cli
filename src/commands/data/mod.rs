use anyhow::Result;
use clap::{Parser, Subcommand};

pub mod characters;
pub mod covers;
pub mod genres;
pub mod sources;

/// Performs operations on a project in the current directory.
#[derive(Parser, Debug)]
pub struct DataCommand {
    #[clap(subcommand)]
    pub cmd: DataCommands,
}

#[derive(Subcommand, Debug)]
pub enum DataCommands {
    #[clap(name = "characters", about = "Work with characters")]
    Characters(characters::CharactersCommand),

    #[clap(name = "covers", about = "Work with charactercovers")]
    Covers(covers::CoversCommand),

    #[clap(name = "genres", about = "Work with genres")]
    Genres(genres::GenresCommand),

    #[clap(name = "sources", about = "Work with sources")]
    Sources(sources::SourcesCommand),
}

impl DataCommand {
    /// Executes the `project` command.
    pub async fn run(&self) -> Result<()> {
        match &self.cmd {
            DataCommands::Characters(cmd) => cmd.run().await?,
            DataCommands::Covers(cmd) => cmd.run().await?,
            DataCommands::Genres(cmd) => cmd.run().await?,
            DataCommands::Sources(cmd) => cmd.run().await?,
        };

        Ok(())
    }
}
