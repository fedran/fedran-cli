use crate::data::covers::CoverData;
use anyhow::Result;
use clap::Parser;
use prettytable::*;

/// Lists all the covers known to the system.
#[derive(Parser, Debug)]
pub struct CoversCommand {}

impl CoversCommand {
    /// Executes the configuration sub-command.
    pub async fn run(&self) -> Result<()> {
        // Get the list of covers.
        let covers = CoverData::get().await?;

        // Set up the table and formatting.
        let mut table = Table::new();

        table.set_format(*format::consts::FORMAT_NO_BORDER_LINE_SEPARATOR);
        table.set_titles(row!["identifier", "title_slug", "cover_slug"]);

        // Go through and display each character.
        for cover in &covers.covers {
            let cover = cover.borrow();

            table.add_row(row![
                cover.format_identifier(),
                cover.title_slug,
                cover.get_cover_slug()
            ]);
        }

        // Render the table.
        table.printstd();
        Ok(())
    }
}
