use crate::data::sources::SourceData;
use anyhow::Result;
use clap::Parser;
use prettytable::*;

/// Lists all the characters known to the identifiers.
#[derive(Parser, Debug)]
pub struct CharactersCommand {}

impl CharactersCommand {
    /// Executes the configuration sub-command.
    pub async fn run(&self) -> Result<()> {
        // Get the sources from a local drive or remotely.
        let sources = SourceData::get().await?;

        // Set up the table and formatting.
        let mut table = Table::new();

        table.set_format(*format::consts::FORMAT_NO_BORDER_LINE_SEPARATOR);
        table.set_titles(row!["pov", "character", "slug", "sources"]);

        // Go through and display each character.
        for character in &sources.characters {
            let character = character.borrow();
            let source_count = &sources
                .sources
                .iter()
                .filter(|x| x.borrow().pov == character.pov)
                .count();
            let source_count = match source_count {
                0 => "".to_string(),
                _ => source_count.to_string(),
            };
            table.add_row(row![
                character.format_pov(),
                character.name,
                character.get_slug(),
                source_count
            ]);
        }

        // Render the table.
        table.printstd();
        Ok(())
    }
}
