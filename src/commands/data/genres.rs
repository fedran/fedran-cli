use crate::data::genres::GenreData;
use anyhow::Result;
use clap::Parser;
use prettytable::*;

/// Lists all the genres known to the identifiers.
#[derive(Parser, Debug)]
pub struct GenresCommand {}

impl GenresCommand {
    /// Executes the configuration sub-command.
    pub async fn run(&self) -> Result<()> {
        // Get the sources from a local drive or remotely.
        let sources = GenreData::get().await?;

        // Set up the table and formatting.
        let mut table = Table::new();

        table.set_format(*format::consts::FORMAT_NO_BORDER_LINE_SEPARATOR);
        table.set_titles(row!["name", "summary"]);

        // Go through and display each genre.
        for genre in &sources.genres {
            let genre = genre.borrow();
            table.add_row(row![genre.name, genre.summary,]);
        }

        // Render the table.
        table.printstd();
        Ok(())
    }
}
