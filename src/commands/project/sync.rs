use super::ProjectCommandState;
use crate::data::projects::ProjectData;
use crate::data::sources::SourceData;
use anyhow::{bail, Result};
use clap::Parser;
use std::cell::RefCell;
use std::rc::Rc;

#[derive(Parser, Debug)]
pub struct ProjectSyncCommand {}

/// Rebuilds the 'fedran.json' file in the project root and synchronizes data with the shared server
/// to include th enew information
impl ProjectSyncCommand {
    pub async fn run(&self, project: ProjectData) -> Result<ProjectCommandState> {
        // Do some initial checking of the project. If we don't have the character and volume, then
        // there is nothing we can do.
        if project.character.is_none() || project.source.is_none() {
            bail!("cannot sync project without an assigned character and volume");
        }

        // Pull out the project information.
        let project_data = project.clone();
        let project_source = project_data.source.unwrap();
        let project_source = project_source.borrow();
        let mut project_source = project_source.clone();

        // We don't upload various components as they are too noisy.
        project_source.chapters = None;
        project_source.credits = None;

        // Get the characters and sources from the server.
        let mut data = SourceData::get().await?;

        log::info!("updating source in metadata");
        data = data.set_source(Rc::new(RefCell::new(project_source.clone())));

        // If we made changes to the sources, we need to commit it.
        log::info!("updating remote sources");
        data.commit(format!(
            "feat({}): updating source data",
            project_source.get_slug()
        ))
        .await?;

        // There are no changes to the project.
        Ok(ProjectCommandState::no_change())
    }
}
