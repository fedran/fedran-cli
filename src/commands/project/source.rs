use super::ProjectCommandState;
use crate::data::projects::ProjectData;
use crate::data::sources::{Source, SourceData};
use anyhow::{Error, Result};
use clap::Parser;
use inquire::Select;
use std::cell::RefCell;
use std::env::current_dir;
use std::rc::Rc;

#[derive(Parser, Debug)]
pub struct ProjectVolumeCommand {
    #[clap(subcommand)]
    pub op: Option<Operation>,
}

#[derive(Parser, Debug)]
pub enum Operation {
    #[clap(name = "title", about = "Display the title of the volume")]
    Title,
    #[clap(name = "identifier", about = "Display the formatted identifier")]
    Identifier,
    #[clap(name = "volume", about = "Display the two-digit POV number")]
    Volume,
    #[clap(name = "slug", about = "Display the slut for the character")]
    Slug,
    #[clap(name = "url", about = "Display the URL for the character")]
    Url,
    #[clap(name = "set", about = "Sets the character")]
    Set,
    #[clap(name = "guess", about = "Extrapolate the source of the project")]
    Guess,
}

impl ProjectVolumeCommand {
    pub async fn run(&self, mut project: ProjectData) -> Result<ProjectCommandState> {
        // Figure out what to do based on the command.
        let changed = match self.op {
            Some(Operation::Set) => self.set(&mut project).await?,
            Some(Operation::Guess) => self.guess(&mut project).await?,
            Some(Operation::Identifier) => self.print_identifier(&mut project),
            Some(Operation::Volume) => self.print_volume(&mut project),
            Some(Operation::Slug) => self.print_slug(&mut project),
            Some(Operation::Url) => self.print_url(&mut project),
            _ => self.print_source(&mut project),
        };

        // Return the project if we edited it.
        Ok(ProjectCommandState {
            project: match changed {
                true => Some(project),
                false => None,
            },
        })
    }

    /// Attempts to guess the project source based on information provide. If the source is already
    /// set, then nothing will be done. If the project cannot be guessed, then this program will
    /// bail.
    async fn guess(&self, project: &mut ProjectData) -> Result<bool> {
        // If it is already set, then nothing to change.
        if project.source.is_some() {
            log::info!("project source is already set");

            return Ok(false);
        }

        // Get the list of characters and sources.
        let data = SourceData::get().await?;
        let sources = &data.sources;

        // Try to guess it based on the name of the directory.
        let mut source = None;

        if let Some(directory) = current_dir()?.file_name() {
            let directory = directory.to_string_lossy().to_string();
            log::info!("trying directory name {:?}", directory);

            let found = sources
                .iter()
                .filter(|x| x.borrow().get_slug() == directory)
                .next();

            source = match found {
                Some(found_source) => {
                    log::info!("found {}", found_source.borrow());
                    Some(found_source)
                }
                None => {
                    log::warn!("could not find source from directory {}", directory);
                    None
                }
            };
        }

        // If we have a choice, then set it.
        if let Some(choice) = source {
            let choice = Rc::clone(choice);
            let choice = choice.borrow().clone();

            return self.set_source(project, &data, choice);
        }

        // Otherwise, we cannot figure out the project source.
        anyhow::bail!("cannot determine project source");
    }

    async fn set(&self, project: &mut ProjectData) -> Result<bool> {
        // Get the list of characters and sources.
        let data = SourceData::get().await?;
        let sources = &data.sources;

        // If we have a character already chosen, then we filter out the source list to
        // only include those sources that match that character.
        let pov = match &project.character {
            Some(character) => Some(character.borrow_mut().pov),
            None => None,
        };

        // Build up the prompt for the sources based on the character or lack of character.
        let choices = sources
            .iter()
            .filter(|ch| match pov {
                Some(pov) => ch.borrow_mut().pov == pov,
                None => true,
            })
            .map(|x| x.borrow_mut().clone())
            .collect::<Vec<crate::data::sources::Source>>();
        let index = match &project.source {
            Some(current_choice) => {
                let source_index = &choices
                    .iter()
                    .position(|x| x.volume == current_choice.borrow_mut().volume)
                    .unwrap_or(0);
                *source_index
            }
            None => 0,
        };
        let choice = Select::new("Choose:", choices)
            .with_starting_cursor(index)
            .prompt()?;

        // Set the source and, possibly, the character.
        self.set_source(project, &data, choice)
    }

    fn set_source(
        &self,
        project: &mut ProjectData,
        data: &SourceData,
        choice: Source,
    ) -> Result<bool, Error> {
        // Figure out the character for this choice.
        let character = &data.get_character_from_pov(choice.pov);

        if let Some(character) = character {
            // Get a second reference to the character.
            let character = Rc::clone(character);

            // Set the character and source.
            let source = RefCell::new(choice);
            let source = Rc::new(source);

            project.character = Some(character);
            project.identifier = Some(Rc::clone(&source).borrow().format_identifier());
            project.source = Some(source);

            Ok(true)
        } else {
            anyhow::bail!("cannot find a character associated with source");
        }
    }

    fn print_identifier(&self, project: &mut ProjectData) -> bool {
        println!(
            "{}",
            match &project.source {
                Some(source) => source.borrow_mut().format_identifier(),
                None => "<not set>".to_string(),
            }
        );

        false
    }

    fn print_volume(&self, project: &mut ProjectData) -> bool {
        println!(
            "{}",
            match &project.source {
                Some(source) => source.borrow_mut().format_volume(),
                None => "<not set>".to_string(),
            }
        );

        false
    }

    fn print_slug(&self, project: &mut ProjectData) -> bool {
        println!(
            "{}",
            match &project.source {
                Some(source) => source.borrow_mut().get_slug(),
                None => "<not set>".to_string(),
            }
        );

        false
    }

    fn print_url(&self, project: &mut ProjectData) -> bool {
        println!(
            "{}",
            match &project.source {
                Some(source) => source.borrow_mut().get_http_url(),
                None => "<not set>".to_string(),
            }
        );

        false
    }

    fn print_source(&self, project: &mut ProjectData) -> bool {
        println!(
            "{}",
            match &project.source {
                Some(source) => source.borrow_mut().title.clone(),
                None => "<not set>".to_string(),
            }
        );

        false
    }
}
