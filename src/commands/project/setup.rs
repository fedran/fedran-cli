use super::ProjectCommandState;
use crate::config::get_config;
use crate::data::projects::ProjectData;
use crate::data::sources::Source;
use crate::woodpecker::WoodpeckerClient;
use anyhow::{bail, Result};
use clap::Parser;
use handlebars::{handlebars_helper, Handlebars};
use serde::{Deserialize, Serialize};
use std::borrow::Borrow;
use std::cell::RefCell;
use std::fs;
use std::fs::File;
use std::io::Write;
use std::os::unix::fs::PermissionsExt;
use std::path::PathBuf;
use std::rc::Rc;
use std::str::from_utf8;

#[derive(Parser, Debug)]
pub struct ProjectSetupCommand {}

#[derive(Parser, Debug, Clone, Serialize, Deserialize)]
struct TemplateModel {
    title: String,
    title_slug: String,
    title_lines: Vec<String>,
    copyright_year: u16,
    version: String,
    length: String,
    has_cover: bool,
    cover_slug: Option<String>,
    has_miwafu: bool,
    has_lorban: bool,
    has_dedication: bool,
    warnings: String,
    credits: Option<String>,
}

#[derive(Parser, Debug, Clone, Serialize, Deserialize)]
struct PackageJson {
    pub version: Option<String>,
}

// Set up the Handlebars helper.
handlebars_helper!(braces: |s: str| format!("{{{{{}}}}}", s));
handlebars_helper!(lower: |s: str| s.to_lowercase());

/// Sets up the project file, populating the various elements and components needed to ensure a
/// consistent project setup.
impl ProjectSetupCommand {
    pub async fn run(
        &self,
        project: ProjectData,
        project_dir: &PathBuf,
    ) -> Result<ProjectCommandState> {
        // Do some initial checking of the project. If we don't have the character and volume, then
        // there is nothing we can do.
        if project.character.is_none() || project.source.is_none() {
            bail!("cannot setup project without an assigned character and volume");
        }

        // See if we already have a version in the package.json file to use.
        let source = project.source.unwrap();
        let source = Rc::borrow(&source);
        let source = RefCell::borrow(source);
        let source = source.clone();
        let package_json = self.load_package_json(project_dir)?;
        let title = source.title.clone();
        let model = TemplateModel {
            title: source.clone().title,
            title_slug: source.clone().title_slug.unwrap(),
            title_lines: match &source.cover {
                None => vec![title.to_owned()],
                Some(cover) => match &cover.title_page_lines {
                    None => vec![title.to_owned()],
                    Some(lines) => lines.to_vec(),
                },
            },
            copyright_year: match &source.copyright_year {
                None => panic!("need a copyright year!"),
                Some(year) => *year,
            },
            version: package_json.version.unwrap(),
            length: format!("{:?}", source.clone().length.unwrap()),
            has_cover: match &source.cover {
                None => false,
                Some(cover) => match &cover.cover_slug {
                    None => false,
                    _ => true,
                },
            },
            cover_slug: match &source.cover {
                None => None,
                Some(cover) => match &cover.cover_slug {
                    None => None,
                    Some(cover_slug) => Some(cover_slug.to_owned()),
                },
            },
            has_miwafu: match &source.languages {
                None => false,
                Some(languages) => match &languages.miwafu {
                    None => false,
                    _ => true,
                },
            },
            has_lorban: match &source.languages {
                None => false,
                Some(languages) => match &languages.lorban {
                    None => false,
                    _ => true,
                },
            },
            has_dedication: match &source.has_dedication {
                None => false,
                Some(dedication) => *dedication,
            },
            warnings: match &source.warnings {
                None => "".to_string(),
                Some(warnings) => match &warnings.text {
                    None => "".to_string(),
                    Some(text) => text.to_owned(),
                },
            },
            credits: self.create_credits(source.clone()),
        };

        // Make sure we have the right directories.
        self.create_dirs(project_dir)?;

        // Perform the various changes required for the project.
        self.copy_static_files(project_dir, &model)?;
        self.apply_template_files(project_dir, &model)?;

        // Make sure Woodpecker CI is set up properly.
        self.setup_woodpecker(model).await?;

        // There are no changes to the project.
        Ok(ProjectCommandState::no_change())
    }

    /// Applies a Handlebars template against the project and a template file.
    fn apply(
        &self,
        project_dir: &PathBuf,
        model: &TemplateModel,
        template_name: &str,
        template_bytes: &[u8],
        output_path: &str,
    ) -> Result<()> {
        // Figure out what we're writing out.
        let output_path = project_dir.join(output_path);

        log::info!(
            "applying {} ({} bytes) to {:?}",
            template_name,
            template_bytes.len(),
            output_path
        );

        // Set up the Handlebars engine.
        let mut engine = Handlebars::new();

        engine.register_helper("braces", Box::new(braces));
        engine.register_helper("lower", Box::new(lower));

        // Render the template.
        let template = from_utf8(template_bytes)?;
        let template = template.to_string();
        let output = engine.render_template(&template, &model)?;

        // Write the template out to the file.
        let output_bytes = output.as_bytes();
        let mut file = File::create(output_path)?;

        file.write_all(output_bytes)?;

        Ok(())
    }

    fn apply_script(
        &self,
        project_dir: &PathBuf,
        model: &TemplateModel,
        template_name: &str,
        template_bytes: &[u8],
        output_path: &str,
    ) -> Result<()> {
        let output_path = output_path.to_string();
        let _ = &self.apply(
            project_dir,
            model,
            template_name,
            template_bytes,
            &output_path,
        )?;

        let output_path = project_dir.join(output_path);
        fs::set_permissions(output_path, fs::Permissions::from_mode(0o755)).unwrap();
        Ok(())
    }

    /// Applies all the template changes needed for a project.
    fn apply_template_files(&self, project_dir: &PathBuf, model: &TemplateModel) -> Result<()> {
        self.apply(
            project_dir,
            model,
            "package.json.hbs",
            include_bytes!("../../../tmpl/package.json.hbs"),
            "package.json",
        )?;
        self.apply(
            project_dir,
            model,
            "package-lock.json.hbs",
            include_bytes!("../../../tmpl/package-lock.json.hbs"),
            "package-lock.json",
        )?;
        self.apply(
            project_dir,
            model,
            "publication.json.hbs",
            include_bytes!("../../../tmpl/publication.json.hbs"),
            "publication.json",
        )?;

        self.apply(
            project_dir,
            model,
            "matter/legal.md.hbs",
            include_bytes!("../../../tmpl/matter/legal.md.hbs"),
            "matter/legal.md",
        )?;
        self.apply(
            project_dir,
            model,
            "matter/license.md.hbs",
            include_bytes!("../../../tmpl/matter/license.md.hbs"),
            "matter/license.md",
        )?;
        self.apply(
            project_dir,
            model,
            "matter/title.html.hbs",
            include_bytes!("../../../tmpl/matter/title.html.hbs"),
            "matter/title.html",
        )?;

        if model.credits.is_some() {
            self.apply(
                project_dir,
                model,
                "matter/credits.md.hbs",
                include_bytes!("../../../tmpl/matter/credits.md.hbs"),
                "matter/credits.md",
            )?;
        } else {
            self.delete(project_dir, "matter/credits.md")?;
        }

        self.apply_script(
            project_dir,
            model,
            "scripts/setup.sh.hbs",
            include_bytes!("../../../tmpl/scripts/setup.sh.hbs"),
            "scripts/setup.sh",
        )?;
        self.apply_script(
            project_dir,
            model,
            "scripts/release.sh.hbs",
            include_bytes!("../../../tmpl/scripts/release.sh.hbs"),
            "scripts/release.sh",
        )?;

        Ok(())
    }

    /// Copies a file without any substitutions.
    fn copy(
        &self,
        project_dir: &PathBuf,
        template_name: &str,
        template_bytes: &[u8],
        output_path: &str,
    ) -> Result<()> {
        let output_path = project_dir.join(output_path);

        log::info!(
            "copying {} ({} bytes) to {:?}",
            template_name,
            template_bytes.len(),
            output_path
        );

        let mut file = File::create(output_path)?;

        file.write_all(template_bytes)?;

        Ok(())
    }

    /// Copies a file without any substitutions.
    fn copy_script(
        &self,
        project_dir: &PathBuf,
        template_name: &str,
        template_bytes: &[u8],
        output_path: &str,
    ) -> Result<()> {
        let output_path = output_path.to_string();
        let _ = &self.copy(project_dir, template_name, template_bytes, &output_path)?;
        let output_path = project_dir.join(output_path);
        fs::set_permissions(output_path, fs::Permissions::from_mode(0o755)).unwrap();
        Ok(())
    }

    /// Copies the various static files that don't change based on the project.
    fn copy_static_files(&self, project_dir: &PathBuf, model: &TemplateModel) -> Result<()> {
        self.copy(
            project_dir,
            "commitlint.config.js.hbs",
            include_bytes!("../../../tmpl/commitlint.config.js.hbs"),
            "commitlint.config.js",
        )?;
        self.copy(
            project_dir,
            "editorconfig.hbs",
            include_bytes!("../../../tmpl/editorconfig.hbs"),
            ".editorconfig",
        )?;
        self.copy(
            project_dir,
            "envrc.hbs",
            include_bytes!("../../../tmpl/envrc.hbs"),
            ".envrc",
        )?;
        self.copy(
            project_dir,
            "flake.lock.hbs",
            include_bytes!("../../../tmpl/flake.lock.hbs"),
            "flake.lock",
        )?;
        self.copy(
            project_dir,
            "flake.nix.hbs",
            include_bytes!("../../../tmpl/flake.nix.hbs"),
            "flake.nix",
        )?;
        self.copy(
            project_dir,
            "gitignore.hbs",
            include_bytes!("../../../tmpl/gitignore.hbs"),
            ".gitignore",
        )?;
        self.copy(
            project_dir,
            "prettierignore.hbs",
            include_bytes!("../../../tmpl/prettierignore.hbs"),
            ".prettierignore",
        )?;
        self.copy(
            project_dir,
            "release.config.js.hbs",
            include_bytes!("../../../tmpl/release.config.js.hbs"),
            "release.config.js",
        )?;
        self.copy(
            project_dir,
            "woodpecker.tmpl",
            include_bytes!("../../../tmpl/woodpecker.tmpl"),
            ".woodpecker.yml",
        )?;

        self.copy(
            project_dir,
            "matter/about.md.tmpl",
            include_bytes!("../../../tmpl/matter/about.md.tmpl"),
            "matter/about.md",
        )?;
        self.copy(
            project_dir,
            "matter/fedran.md.tmpl",
            include_bytes!("../../../tmpl/matter/fedran.md.tmpl"),
            "matter/fedran.md",
        )?;
        self.copy(
            project_dir,
            "matter/pad.html.tmpl",
            include_bytes!("../../../tmpl/matter/pad.html.tmpl"),
            "matter/pad.html",
        )?;

        if model.has_miwafu {
            self.copy(
                project_dir,
                "matter/miwafu.html.tmpl",
                include_bytes!("../../../tmpl/matter/miwafu.html.tmpl"),
                "matter/miwafu.html",
            )?;
        } else {
            self.delete(project_dir, "matter/miwafu.html")?;
        }

        if model.has_lorban {
            self.copy(
                project_dir,
                "matter/lorban.html.tmpl",
                include_bytes!("../../../tmpl/matter/lorban.html.tmpl"),
                "matter/lorban.html",
            )?;
        } else {
            self.delete(project_dir, "matter/lorban.html")?;
        }

        self.copy_script(
            project_dir,
            "scripts/setup-fonts.sh.tmpl",
            include_bytes!("../../../tmpl/scripts/setup-fonts.sh.tmpl"),
            "scripts/setup-fonts.sh",
        )?;
        self.copy_script(
            project_dir,
            "scripts/setup-node.sh.tmpl",
            include_bytes!("../../../tmpl/scripts/setup-node.sh.tmpl"),
            "scripts/setup-node.sh",
        )?;
        self.copy_script(
            project_dir,
            "source/check-env-bucket.sh.tmpl",
            include_bytes!("../../../tmpl/scripts/check-env-bucket.sh.tmpl"),
            "scripts/check-env-bucket.sh",
        )?;
        self.copy_script(
            project_dir,
            "source/build.sh.tmpl",
            include_bytes!("../../../tmpl/scripts/build.sh.tmpl"),
            "scripts/build.sh",
        )?;

        if model.has_cover {
            self.copy_script(
                project_dir,
                "source/setup-fedran-covers.sh.tmpl",
                include_bytes!("../../../tmpl/scripts/setup-fedran-covers.sh.tmpl"),
                "scripts/setup-fedran-covers.sh",
            )?;
        }

        Ok(())
    }

    fn delete(&self, project_dir: &PathBuf, output_path: &str) -> Result<()> {
        let path = project_dir.join(output_path);

        if path.is_file() {
            log::info!("removing file {:?}", path);
            fs::remove_file(path)?;
        }

        Ok(())
    }

    /// Loads the `package.json` file into memory and attempts to pick up the version.
    fn load_package_json(&self, project_dir: &PathBuf) -> Result<PackageJson> {
        let path = project_dir.join("package.json");
        let mut package_json = match path.is_file() {
            false => PackageJson { version: None },
            true => {
                let json = fs::read_to_string(&path)?;
                serde_json::from_str(json.as_str())?
            }
        };

        if let Some(version) = &package_json.version {
            log::info!("package version is {:?}", version);
        } else {
            log::info!("assuming package version of 0.0.1");
            package_json.version = Some("0.0.1".to_owned());
        }

        Ok(package_json)
    }
    fn create_dirs(&self, project_dir: &PathBuf) -> Result<()> {
        let matter_dir = project_dir.join("matter");
        let scripts_dir = project_dir.join("scripts");

        if !matter_dir.is_dir() {
            log::info!("creating matter/ directory");
            fs::create_dir(matter_dir)?;
        }

        if !scripts_dir.is_dir() {
            log::info!("creating scripts/ directory");
            fs::create_dir(scripts_dir)?;
        }

        Ok(())
    }

    fn create_credits(&self, source: Source) -> Option<String> {
        // If we don't have credits, there is nothing to write.
        if source.credits.is_none() {
            return None;
        }

        // Try to add each of the credits.
        let credits = source.credits.unwrap();
        let mut buffer = "".to_owned();
        let mut have_credits = false;

        match self.create_credits_block("Alpha Readers", credits.alpha_readers) {
            None => {}
            Some(markdown) => {
                buffer = format!("{}\n{}", buffer, markdown);
                have_credits = true;
            }
        }

        match self.create_credits_block("Editors", credits.editors) {
            None => {}
            Some(markdown) => {
                buffer = format!("{}\n{}", buffer, markdown);
                have_credits = true;
            }
        }

        match self.create_credits_block("Beta Readers", credits.beta_readers) {
            None => {}
            Some(markdown) => {
                buffer = format!("{}\n{}", buffer, markdown);
                have_credits = true;
            }
        }

        match self.create_credits_block("Sensitivity Readers", credits.sensitivity_readers) {
            None => {}
            Some(markdown) => {
                buffer = format!("{}\n{}", buffer, markdown);
                have_credits = true;
            }
        }

        // If we didn't have any credits, we have none.
        match have_credits {
            false => None,
            true => Some(buffer),
        }
    }

    fn create_credits_block(&self, title: &str, names: Option<Vec<String>>) -> Option<String> {
        if names.is_none() {
            return None;
        }

        let names = names.unwrap();
        let mut buffer = format!("## {}\n\n| ---------- | --------- | ----------\n", title);

        for chunk in names.chunks(3) {
            // First add up the four names in the list.
            for name in chunk {
                buffer = format!("{}| {} ", buffer, name);
            }

            // Finish up with some empty braces.
            for _ in chunk.len()..3 {
                buffer = format!("{}|", buffer);
            }

            // Finish up the line.
            buffer = format!("{}|\n", buffer);
        }

        Some(buffer)
    }

    async fn setup_woodpecker(&self, model: TemplateModel) -> Result<()> {
        // Create a client and make sure we can connect.
        let config = get_config()?;
        let woodpecker = WoodpeckerClient::new()?;

        log::debug!("woodpecker user {:?}", woodpecker.get_user().await?.id);

        // Get information about this specific repository.
        let repo = woodpecker
            .get_repo("fedran-sources", &model.title_slug)
            .await?;

        if !repo.active {
            log::warn!("woodpecker: repository is not active");
            woodpecker
                .set_repo_active("fedran-sources", &model.title_slug, true, true)
                .await?;
        }

        // Set up the secrets.
        let gitea_token = &config.gitea_token.unwrap();

        log::info!("woodpecker: setting up secrets");
        self.setup_woodpecker_secret(&woodpecker, &model, "gitea_token", &gitea_token)
            .await?;
        self.setup_woodpecker_secret(&woodpecker, &model, "s3_bucket", &config.s3_bucket.unwrap())
            .await?;
        self.setup_woodpecker_secret(
            &woodpecker,
            &model,
            "s3_endpoint",
            &config.s3_endpoint.unwrap(),
        )
        .await?;
        self.setup_woodpecker_secret(
            &woodpecker,
            &model,
            "s3_access_key_id",
            &config.s3_access_key.unwrap(),
        )
        .await?;
        self.setup_woodpecker_secret(
            &woodpecker,
            &model,
            "s3_secret_access_key",
            &config.s3_secret_key.unwrap(),
        )
        .await?;

        Ok(())
    }

    async fn setup_woodpecker_secret(
        &self,
        woodpecker: &WoodpeckerClient,
        model: &TemplateModel,
        name: &str,
        value: &str,
    ) -> Result<()> {
        log::info!("woodpecker: setting up secret: {}", name);

        woodpecker
            .set_repo_secret("fedran-sources", &model.title_slug, name, value)
            .await?;

        Ok(())
    }
}
