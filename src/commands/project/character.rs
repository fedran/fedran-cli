use super::ProjectCommandState;
use crate::data::{projects::ProjectData, sources::SourceData};
use anyhow::Result;
use clap::Parser;
use inquire::Select;
use std::cell::RefCell;
use std::rc::Rc;

#[derive(Parser, Debug)]
pub struct ProjectCharacterCommand {
    #[clap(subcommand)]
    pub op: Option<Operation>,
}

#[derive(Parser, Debug)]
pub enum Operation {
    #[clap(name = "name", about = "Display the name of the character")]
    Name,
    #[clap(name = "pov", about = "Display the four-digit POV number")]
    Pov,
    #[clap(name = "slug", about = "Display the slut for the character")]
    Slug,
    #[clap(name = "url", about = "Display the URL for the character")]
    Url,
    #[clap(name = "set", about = "Sets the character")]
    Set,
}

impl ProjectCharacterCommand {
    pub async fn run(&self, mut project: ProjectData) -> Result<ProjectCommandState> {
        // Figure out what to do based on the command which will determine if we made a change.
        let changed = match self.op {
            Some(Operation::Set) => self.set(&mut project).await?,
            Some(Operation::Pov) => self.print_pov(&mut project),
            Some(Operation::Slug) => self.print_slug(&mut project),
            Some(Operation::Url) => self.print_url(&mut project),
            _ => self.print_name(&mut project),
        };

        // Return the project if we edited it.
        Ok(ProjectCommandState {
            project: match changed {
                true => Some(project),
                false => None,
            },
        })
    }

    async fn set(&self, project: &mut ProjectData) -> Result<bool> {
        // Get the list of characters.
        let sources = SourceData::get().await?;
        let characters = sources.characters;
        let choices = characters
            .iter()
            .map(|x| x.borrow_mut().clone())
            .collect::<Vec<crate::data::characters::Character>>();
        let index = match &project.character {
            Some(current_choice) => {
                let source_index = &choices
                    .iter()
                    .position(|x| x.pov == current_choice.borrow_mut().pov)
                    .unwrap_or(0);
                *source_index
            }
            None => 0,
        };

        // Prompt the user and get their choice.
        let choice = Select::new("Choose:", choices)
            .with_starting_cursor(index)
            .prompt()?;

        // Set the character and clear out the source (but only if the new character is
        // different.
        let character = RefCell::new(choice);
        let character = Rc::new(character);

        project.source = match &project.source {
            None => None,
            Some(source) => {
                let source_pov = { Rc::clone(&source).borrow_mut().pov };
                match source_pov == character.borrow_mut().pov {
                    true => Some(Rc::clone(source)),
                    false => {
                        log::warn!("clearing out project source from different character");
                        None
                    }
                }
            }
        };
        project.identifier = match &project.source {
            None => None,
            Some(source) => Some(source.borrow().format_identifier()),
        };
        project.character = Some(character);

        Ok(true)
    }

    fn print_pov(&self, project: &mut ProjectData) -> bool {
        println!(
            "{}",
            match &project.character {
                Some(character) => character.borrow_mut().format_pov(),
                None => "<not set>".to_string(),
            }
        );

        false
    }

    fn print_slug(&self, project: &mut ProjectData) -> bool {
        println!(
            "{}",
            match &project.character {
                Some(character) => character.borrow_mut().get_slug(),
                None => "<not set>".to_string(),
            }
        );

        false
    }

    fn print_url(&self, project: &mut ProjectData) -> bool {
        println!(
            "{}",
            match &project.character {
                Some(character) => character.borrow_mut().get_url(),
                None => "<not set>".to_string(),
            }
        );

        false
    }

    fn print_name(&self, project: &mut ProjectData) -> bool {
        println!(
            "{}",
            match &project.character {
                Some(character) => character.borrow_mut().name.clone(),
                None => "<not set>".to_string(),
            }
        );

        false
    }
}
