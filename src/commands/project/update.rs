use super::ProjectCommandState;
use crate::commands::project::sync::ProjectSyncCommand;
use crate::data::chapters::Chapter;
use crate::data::covers::CoverData;
use crate::data::projects::ProjectData;
use crate::data::sources::covers::SourceCover;
use crate::data::sources::description::SourceDescription;
use crate::data::sources::length::LengthCategory;
use crate::data::sources::links::{DevelopmentSourceLinks, SourceLinks};
use crate::data::sources::status::SourceStatus;
use crate::data::sources::{Source, SourceGit};
use crate::data::warnings::SourceWarning;
use anyhow::{bail, Result};
use chrono::Datelike;
use chrono::NaiveDateTime;
use clap::Parser;
use git2::Repository;
use glob::glob;
use std::cell::RefMut;
use std::fs;
use std::path::PathBuf;
use std::rc::Rc;
use textwrap::wrap_algorithms::Penalties;
use textwrap::{Options, WrapAlgorithm};
use yaml_front_matter::YamlFrontMatter;

#[derive(Parser, Debug)]
pub struct ProjectUpdateCommand {
    /// If true, then force regenerating the title page lines.
    #[arg(long)]
    pub force_title_page_lines: bool,

    /// If true, then also sync the data.
    #[arg(long)]
    pub sync: bool,
}

/// Rebuilds the 'fedran.json' file in the project root.
impl ProjectUpdateCommand {
    pub async fn run(
        &self,
        project: ProjectData,
        project_dir: &PathBuf,
    ) -> Result<ProjectCommandState> {
        // Do some initial checking of the project. If we don't have the character and volume, then
        // there is nothing we can do.
        if project.character.is_none() || project.source.is_none() {
            bail!("cannot sync project without an assigned character and volume");
        }

        // Update the project with the appropriate components and elements. We'll write out the
        // results just in case we made any changes.
        let project = self.update_project(project, project_dir).await?;

        // Check to see if we need to sync.
        if self.sync {
            log::info!("calling sync");
            let sync_command = ProjectSyncCommand {};
            sync_command.run(project.clone()).await?;
        }

        // Return the updated project.
        return Ok(ProjectCommandState {
            project: Some(project),
        });
    }

    async fn update_project(
        &self,
        project: ProjectData,
        project_dir: &PathBuf,
    ) -> Result<ProjectData> {
        self.update_project_character(&project);
        self.update_project_source(&project, project_dir).await?;

        Ok(project)
    }

    fn update_project_character(&self, project: &ProjectData) {
        // Because this is Rc<RefCell<Character>>, any changes we make are self-contained.
        if let Some(project_character) = &project.character {
            let project_character = Rc::clone(project_character);
            let mut project_character = project_character.borrow_mut();

            // If we don't have a slug, then provide it.
            if project_character.character_slug.is_none() {
                log::info!("setting character slug to {}", project_character.get_slug());
                project_character.character_slug = Some(project_character.get_slug());
            }
        }
    }

    async fn update_project_source(
        &self,
        project: &ProjectData,
        project_dir: &PathBuf,
    ) -> Result<()> {
        // Because this is Rc<RefCell<Source>>, any changes we make are self-contained.
        if let Some(project_source) = &project.source {
            let project_source = Rc::clone(project_source);
            let mut project_source = project_source.borrow_mut();

            // We want to start by updating the chapters from the file. Then we also total up
            // the chapter word counts if possible.
            project_source.chapters = self.update_project_source_chapters(&project_dir)?;
            project_source.word_count = match &project_source.chapters {
                None => None,
                Some(chapters) => chapters.iter().map(|c| c.word_count).sum(),
            };
            project_source.length = match &project_source.word_count {
                None => None,
                Some(count) => {
                    if count < &1000usize {
                        Some(LengthCategory::Flash)
                    } else if count < &10000usize {
                        Some(LengthCategory::Story)
                    } else if count < &20000usize {
                        Some(LengthCategory::Novelette)
                    } else if count < &50000usize {
                        Some(LengthCategory::Novella)
                    } else {
                        Some(LengthCategory::Novel)
                    }
                }
            };
            project_source.warnings = match &project_source.chapters {
                None => None,
                Some(chapters) => Some(SourceWarning::from_sets(
                    chapters.iter().map(|c| &c.warnings).collect(),
                    &project_source.length,
                )),
            };

            // Update the various child components.
            project_source.copyright_year =
                self.update_project_source_copyright(&project_source, project_dir)?;
            project_source.git = self.update_project_source_git(&project_source);
            project_source.links = self.update_project_source_links(&project_source);
            project_source.cover = self.update_project_source_cover(&project_source).await?;
            project_source.description =
                self.update_project_source_description(project_dir).await?;
            project_source.title_slug = Some(project_source.get_slug());

            // Finish up with some additional calculated fields.
            project_source.identifier_slug = Some(format!(
                "{}-{}",
                project_source.format_identifier(),
                project_source.get_slug()
            ));
            project_source.has_dedication =
                Some(project_dir.join("matter").join("dedication.md").is_file());

            // If we don't have a status, it defaults to "Unstable".
            if project_source.status.is_none() {
                log::info!("normalizing status to unstable");
                project_source.status = Some(SourceStatus::Unstable);
            }
        }

        Ok(())
    }

    /// Loads all of the chapters into memory (and in sequence). This assumes that chapters are
    /// either two or three digit chapters ending in `.md` or `.markdown`.
    fn update_project_source_chapters(
        &self,
        project_dir: &PathBuf,
    ) -> Result<Option<Vec<Chapter>>> {
        // Everything is inside the chapters directory. If it doesn't exist, then we have no
        // chapters at all.
        let chapters_dir = project_dir.join("chapters");

        if !chapters_dir.is_dir() {
            log::info!("no chapters/ directory, no chapters");
            return Ok(None);
        }

        // Use `glob` to pick up all the chapters and go through each one.
        let pattern = project_dir.clone().into_os_string().into_string().unwrap();
        let pattern = pattern + "/chapters/chapter-*.m*";
        let mut chapters: Vec<Chapter> = vec![];

        for entry in glob(pattern.as_str())? {
            match entry {
                Ok(path) => chapters.push(Chapter::from_file(path)?),
                Err(e) => log::error!("Cannot read chapter file: {:?}", e),
            }
        }

        // Nothing to return for now.
        Ok(Some(chapters))
    }

    fn update_project_source_copyright(
        &self,
        project_source: &RefMut<Source>,
        project_dir: &PathBuf,
    ) -> Result<Option<u16>> {
        // If we already have a copyright year, there is nothing to else we have to do.
        if let Some(year) = project_source.copyright_year {
            return Ok(Some(year));
        }

        // We need to figure out the copyright year. This is done by basically looking at the last
        // `fix:` or `feat:` commit and using that to infer it.
        log::info!("guessing copyright year from Git repository");

        let repo = Repository::open(project_dir)?;

        // Walk through the commits instead, starting with the first one.
        let mut walk = repo.revwalk()?;
        let spec = repo.revparse("HEAD")?;

        walk.set_sorting(git2::Sort::REVERSE | git2::Sort::TIME)?;
        walk.push_head()?;

        for commit in walk {
            let commit = commit?;
            let commit = repo.find_commit(commit)?;

            if let Some(summary) = commit.summary() {
                // Make sure it is a `feat:` or `fix:`.
                let summary = summary.to_string();

                if !summary.starts_with("fix") && !summary.starts_with("feat") {
                    continue;
                }

                // Figure out the year.
                let time = commit.time();
                let time = time.seconds();
                let time = NaiveDateTime::from_timestamp(time, 0);
                let year = time.year();
                let year: u16 = year as u16;

                log::debug!("commit {:?}: {:?}: {:?}", commit.id(), year, summary);

                return Ok(Some(year));
            }
        }

        // If we get this far, just return none.
        Ok(None)
    }

    fn update_project_source_git(&self, project_source: &RefMut<Source>) -> Option<SourceGit> {
        // Get the Git or create a new one.
        let mut git = match &project_source.git {
            None => SourceGit::new(),
            Some(existing) => existing.clone(),
        };

        // The `http_url` is calculated from the source.
        git.http_url = Some(project_source.get_git_http_url());

        // If we don't have a branch, assume main.
        if git.branch.is_none() {
            git.branch = Some("main".to_string());
        }

        // Return the resulting Git.
        Some(git)
    }

    fn update_project_source_links(&self, project_source: &RefMut<Source>) -> Option<SourceLinks> {
        // Get the Git or create a new one.
        let mut links = match &project_source.links {
            None => SourceLinks::new(),
            Some(existing) => existing.clone(),
        };

        // Development links are hard-coded and don't need to be manipulated.
        links.development = Some(DevelopmentSourceLinks {
            git: Some(project_source.get_git_url()),
            issues: Some(format!("{}issues", project_source.get_git_url())),
        });

        // Return the resulting Git.
        Some(links)
    }

    async fn update_project_source_cover(
        &self,
        project_source: &RefMut<'_, Source>,
    ) -> Result<Option<SourceCover>> {
        // Determine how we want to break up the lines for the title and bastard pages.
        let title = &project_source.title;
        let wrap_options =
            Options::new(12).wrap_algorithm(WrapAlgorithm::OptimalFit(Penalties::new()));
        let proposed_lines = textwrap::wrap(title, wrap_options)
            .into_iter()
            .map(|a| a.into_owned())
            .collect();
        let title_page_lines = match self.force_title_page_lines {
            true => Some(proposed_lines),
            false => match &project_source.cover {
                None => Some(proposed_lines),
                Some(old_cover) => match &old_cover.title_page_lines {
                    None => Some(proposed_lines),
                    Some(lines) => Some(lines.to_vec()),
                },
            },
        };

        // Determines if we have a cover.
        let covers = CoverData::get().await?;
        let cover = covers.get_from_pov_and_volume(project_source.pov, project_source.volume);
        let cover_slug = match &cover {
            None => None,
            Some(cover) => {
                let cover = Rc::clone(cover);
                let cover = cover.borrow_mut();

                Some(cover.get_cover_slug())
            }
        };

        // Return the resulting object which will always have the title information.
        return Ok(Some(SourceCover {
            title_page_lines,
            cover_slug,
        }));
    }

    async fn update_project_source_description(
        &self,
        project_dir: &PathBuf,
    ) -> Result<Option<SourceDescription>> {
        // The standard is to have a `description.md` in the directory.
        let description_path = project_dir.join("description.md");

        if !description_path.is_file() {
            log::info!("no description.md, no description");
            return Ok(None);
        }

        // Load the entire file into memory.
        let description = fs::read_to_string(description_path)?;
        let description = YamlFrontMatter::parse::<Chapter>(&description).unwrap();
        let description = description.content;
        let mut paragraphs = description
            .split("\n\n")
            .map(|x| x.trim().to_string())
            .collect::<Vec<String>>();

        // Grab the tag line.
        let tag = match paragraphs.len() > 0 && paragraphs[0].contains("**") {
            false => None,
            true => Some(paragraphs[0].replace("*", "").trim().to_string()),
        };

        // Get the remaining paragraphs.
        paragraphs.drain(0..1);

        let paragraphs = match paragraphs.len() {
            0 => None,
            _ => Some(paragraphs),
        };

        // Return the results.
        Ok(Some(SourceDescription { tag, paragraphs }))
    }
}
