use crate::data::projects::ProjectData;
use anyhow::Result;
use clap::{Parser, Subcommand};
use find_folder::Search;
use serde::Serialize;
use std::fs;
use std::path::{Path, PathBuf};

pub mod character;
pub mod setup;
pub mod source;
pub mod sync;
pub mod update;

/// Performs operations on a project in the current directory.
#[derive(Parser, Debug)]
pub struct ProjectCommand {
    #[clap(subcommand)]
    pub cmd: ProjectCommands,
}

#[derive(Subcommand, Debug)]
pub enum ProjectCommands {
    #[clap(name = "character", about = "Get or sets the project character")]
    Character(character::ProjectCharacterCommand),

    #[clap(name = "setup", about = "Prepares the project for generating output")]
    Setup(setup::ProjectSetupCommand),

    #[clap(name = "source", about = "Get or sets the project source")]
    Source(source::ProjectVolumeCommand),

    #[clap(name = "sync", about = "Synchronizes the project with the metadata")]
    Sync(sync::ProjectSyncCommand),

    #[clap(name = "update", about = "Updates fedran.json with the local project")]
    Update(update::ProjectUpdateCommand),
}

/// Describes the state after a project command runs.
pub struct ProjectCommandState {
    /// Contains the optional project to write out.
    pub project: Option<ProjectData>,
}

impl ProjectCommand {
    /// Executes the `project` command.
    pub async fn run(&self) -> Result<()> {
        // Look for the .git folder so we can get the root.
        if let Ok(git_dir) = Search::Parents(3).for_folder(".git") {
            // Report our .git folder.
            log::trace!("using git {:?}", git_dir);

            // Figure out the project root.
            let mut project_dir = PathBuf::from(&git_dir);
            project_dir.pop();
            log::info!("using root {:?}", project_dir);

            // Get the `fedran.json` path from the root.
            let mut project_file = PathBuf::from(&git_dir);
            project_file.set_file_name("fedran.json");
            log::debug!("using project {:?}", project_file);

            // Load fedran.json into memory, if it exists.
            let exists = Path::new(&project_file).exists();
            log::debug!("project exists {}", exists);
            let project = match exists {
                true => {
                    let json = fs::read_to_string(&project_file)?;
                    log::info!("loaded project {:?}", &project_file);
                    serde_json::from_str(json.as_str())?
                }
                false => ProjectData::new(),
            };

            log::trace!("project {:?}", project);

            // Delve into the sub-command to perform the actual action.
            let result = match &self.cmd {
                ProjectCommands::Character(cmd) => cmd.run(project).await?,
                ProjectCommands::Setup(cmd) => cmd.run(project, &project_dir).await?,
                ProjectCommands::Source(cmd) => cmd.run(project).await?,
                ProjectCommands::Sync(cmd) => cmd.run(project).await?,
                ProjectCommands::Update(cmd) => cmd.run(project, &project_dir).await?,
            };

            // If the result has a project set, then we write out a new one. We
            // jump through some hoops to get the 4-character indent that the
            // default prettier/.editorconfig setting uses for Fedran projects.
            if let Some(new_project) = result.project {
                log::info!("writing project {:?}", project_file);
                let buf = Vec::new();
                let formatter = serde_json::ser::PrettyFormatter::with_indent(b"    ");
                let mut ser = serde_json::Serializer::with_formatter(buf, formatter);
                new_project.serialize(&mut ser).unwrap();
                fs::write(&project_file, ser.into_inner())?;
            }

            Ok(())
        } else {
            anyhow::bail!("could not find .git root folder")
        }
    }
}

impl ProjectCommandState {
    pub fn no_change() -> ProjectCommandState {
        ProjectCommandState { project: None }
    }
}
