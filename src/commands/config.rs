use crate::config::{get_config_raw, get_config_toml, set_config_toml};
use anyhow::Result;
use clap::Parser;
use std::path::PathBuf;

/// The configuration command for setting up access tokens, paths, and other
/// useful settings.
#[derive(Parser, Debug)]
pub struct ConfigCommand {
    /// If provided, then only list or set the given configuration value.
    #[clap(index = 1)]
    pub name: Option<String>,

    /// If the name is given, then set it to the given value.
    #[clap(index = 2)]
    pub value: Option<String>,

    /// If provided, then clear out the value instead of setting it.
    #[arg(long)]
    pub clear: bool,
}

impl ConfigCommand {
    /// Executes the configuration sub-command.
    pub async fn run(&self) -> Result<()> {
        // Set the value if we have one provided.
        self.set()?;

        // This is one of the few places where we need a raw configuration since
        // we're going to be looping through them.
        let config = get_config_raw()?;

        // Loop through all the configuration settings and get/set them.
        self.print(&config, "gitea_token")?;
        self.print(&config, "woodpecker_token")?;
        self.print(&config, "covers_repo_dir")?;
        self.print(&config, "genres_file")?;
        self.print(&config, "sources_file")?;
        self.print(&config, "s3_bucket")?;
        self.print(&config, "s3_endpoint")?;
        self.print(&config, "s3_access_key")?;
        self.print(&config, "s3_secret_key")?;
        Ok(())
    }

    /// Prints the key/value pair of a configuration.
    fn print(&self, config: &config::Config, key: &str) -> Result<()> {
        // If we have provided a value, then we have some additional work.
        if let Some(name_arg) = &self.name {
            // Check to see if we are filtering out the value.
            if name_arg.ne(key) {
                return Ok(());
            }
        }

        // Normalize the value including masking tokens.
        let value: Option<String> = config.get(key).ok();
        let value = match value {
            Some(x) => match key {
                "gitea_token" => "****".to_string(),
                "woodpecker_token" => "****".to_string(),
                "s3_access_key" => "****".to_string(),
                "s3_secret_key" => "****".to_string(),
                _ => x,
            },
            None => "<unset>".to_string(),
        };

        // Display the results.
        println!("{}: {}", key, value);
        Ok(())
    }

    /// Sets the value if and only if both the name and key are provided.
    fn set(&self) -> Result<()> {
        if let Some(key) = &self.name {
            // Load the configuration into memory without any other config
            // layers being involved.
            let mut toml_config = get_config_toml()?;

            // Figure out if we are clearing or setting.
            match &self.clear {
                true => {
                    log::info!("clearing {}", key);

                    match key.as_str() {
                        "gitea_token" => toml_config.gitea_token = None,
                        "woodpecker_token" => toml_config.woodpecker_token = None,
                        "covers_repo_dir" => toml_config.covers_repo_dir = None,
                        "genres_file" => toml_config.genres_file = None,
                        "sources_file" => toml_config.sources_file = None,
                        "s3_bucket" => toml_config.s3_bucket = None,
                        "s3_endpoint" => toml_config.s3_endpoint = None,
                        "s3_access_key" => toml_config.s3_access_key = None,
                        "s3_secret_key" => toml_config.s3_secret_key = None,
                        _ => log::warn!("unknown configuration key: {}", key),
                    }
                }
                false => {
                    if let Some(value) = &self.value {
                        // Report what we're doing.
                        log::info!("setting {} to {}", key, value);

                        // Update the key including clearing it.
                        match key.as_str() {
                            "gitea_token" => toml_config.gitea_token = Some(value.to_string()),
                            "woodpecker_token" => {
                                toml_config.woodpecker_token = Some(value.to_string())
                            }
                            "covers_repo_dir" => {
                                toml_config.covers_repo_dir = Some(PathBuf::from(value))
                            }
                            "genres_file" => toml_config.genres_file = Some(PathBuf::from(value)),
                            "sources_file" => toml_config.sources_file = Some(PathBuf::from(value)),
                            "s3_bucket" => toml_config.s3_bucket = Some(value.to_string()),
                            "s3_endpoint" => toml_config.s3_endpoint = Some(value.to_string()),
                            "s3_access_key" => toml_config.s3_access_key = Some(value.to_string()),
                            "s3_secret_key" => toml_config.s3_secret_key = Some(value.to_string()),
                            _ => log::warn!("unknown configuration key: {}", key),
                        }
                    }
                }
            }

            // Write out the file.
            set_config_toml(&toml_config)?;
        }

        Ok(())
    }
}
