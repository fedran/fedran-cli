use clap::Parser;

mod commands;
mod config;
mod data;
mod woodpecker;

#[tokio::main]
async fn main() {
    let args = commands::RootCommand::parse();
    let result = args.run().await;

    match result {
        Ok(()) => std::process::exit(0),
        Err(msg) => {
            log::error!("{}", msg);
            std::process::exit(1);
        }
    };
}
