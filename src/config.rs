use anyhow::{Context, Result};
use config::{Config as ConfigConfig, Environment, FileFormat};
use directories::ProjectDirs;
use serde::{Deserialize, Serialize};
use std::fs::{create_dir, File};
use std::io::{Read, Write};
use std::path::{Path, PathBuf};

#[derive(Debug, Deserialize, Serialize)]
pub struct Config {
    /// Contains the optional path to a local sources.
    pub sources_file: Option<PathBuf>,

    /// Contains the optional path to the genre listing.
    pub genres_file: Option<PathBuf>,

    /// Contains the optional path to the warnings data.
    pub warnings_file: Option<PathBuf>,

    /// Contains the optional path to the covers sources.
    pub covers_repo_dir: Option<PathBuf>,

    /// Contains the Gitea token.
    pub gitea_token: Option<String>,

    /// Contains the Woodpecker CI token.
    pub woodpecker_token: Option<String>,

    pub s3_bucket: Option<String>,
    pub s3_endpoint: Option<String>,
    pub s3_access_key: Option<String>,
    pub s3_secret_key: Option<String>,
}

/// Retrieves the path to the configuration file, creating it and any parent
/// directories as needed.
fn get_config_file() -> Result<PathBuf> {
    // If we don't have a configuration, we need to blow up because we don't
    // know how to save or create anything.
    if let Some(proj_dirs) = ProjectDirs::from("com", "fedran", "fedran-cli") {
        // Get the configuration directory for our application.
        let config_dir = proj_dirs.config_dir();
        log::trace!("config_dir: {:?}", config_dir);

        // Make sure the directory exists, creating it if it does not.
        let exists: bool = Path::new(config_dir).is_dir();

        if !exists {
            log::trace!("config_dir does not exist, creating");
            create_dir(config_dir)?;
        }

        // Figure out the name of the file.
        let config_file = config_dir.join("config.toml");
        log::debug!("config_file: {:?}", config_file);

        // Make sure we have an empty configuration file, if one does not
        // already exist.
        let exists: bool = config_file.is_file();

        if !exists {
            log::debug!("config_file does not exist, creating");
            File::create(&config_file).context("unable to create configuration file")?;
        }

        return Ok(config_file);
    } else {
        anyhow::bail!("cannot find standard configuration directory")
    }
}

/// Gets the generic version of the configuration.
pub fn get_config_raw() -> Result<config::Config> {
    // We have a configuration, so load it into memory and return it. We use
    // a blank prefix because we want to pick up "gitlab_token" from CI
    // builds without setting anything new.
    let config_file = get_config_file()?;
    let config_file = config_file.into_os_string();
    let config_file = config_file.into_string().unwrap();

    let builder = ConfigConfig::builder()
        .add_source(config::File::new(&config_file, FileFormat::Toml))
        .add_source(Environment::with_prefix("FEDRAN"));
    let settings = builder.build()?;

    Ok(settings)
}

/// Retrieves the typesafe version of the configuration.
pub fn get_config() -> Result<Config> {
    let settings = get_config_raw()?;
    let config: Config = settings.try_deserialize().unwrap();

    Ok(config)
}

/// Retrieves only the TOML contents of the configuration file without any
/// environment variable.
pub fn get_config_toml() -> Result<Config> {
    // Load the entire file into memory.
    let config_file = get_config_file()?;
    let mut handle = File::open(config_file)?;
    let mut content = String::new();
    handle.read_to_string(&mut content)?;

    // Parse it as a TOML file and return it.
    let config: Config = toml::from_str(&content)?;

    Ok(config)
}

/// Writes out the TOML configuration file.
pub fn set_config_toml(config: &Config) -> Result<Config> {
    // Get a handle to the file to write it out.
    let config_file = get_config_file()?;
    let mut handle = File::create(&config_file)?;

    // Write out the file as a TOML string.
    log::info!("writing out config {:?}", &config_file);
    let content = toml::to_string_pretty(config).unwrap();
    handle.write_all(content.as_bytes())?;

    // Load the file back in.
    get_config_toml()
}
