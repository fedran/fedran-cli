use crate::data::characters::Character;
use crate::data::sources::Source;
use serde::{Deserialize, Serialize};
use std::cell::RefCell;
use std::rc::Rc;

/// Encapsulates the data used for a single project. This is typically stored in the root of each
/// Git repository in a file called `fedran.json`.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct ProjectData {
    /// The file versioning, which is an incrementing value and identifies structural changes to
    /// the file.
    version: u8,

    /// Contains the optional identifier for this source.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub identifier: Option<String>,

    /// Contains the optionally assigned character.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub character: Option<Rc<RefCell<Character>>>,

    /// Contains the optionally assigned source.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub source: Option<Rc<RefCell<Source>>>,
}

impl ProjectData {
    pub fn new() -> ProjectData {
        ProjectData {
            version: 0,
            character: None,
            source: None,
            identifier: None,
        }
    }
}
