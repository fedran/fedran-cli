use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct SourceLinks {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub development: Option<DevelopmentSourceLinks>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub online: Option<OnlineSourceLinks>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub print: Option<PrintSourceLinks>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub digital: Option<DigitalSourceLinks>,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct DevelopmentSourceLinks {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub git: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub issues: Option<String>,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct OnlineSourceLinks {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub wattpad: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub goodreads: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub librarything: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub storygraph: Option<String>,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct PrintSourceLinks {
    /// Contains the latest ISBN of the version being printed.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub isbn: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub amazon: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub bookdepository: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub bn: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub typewriter: Option<String>,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct DigitalSourceLinks {
    /// Contains the latest ISBN of the digital version.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub isbn: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub amazon: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub bn: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub google: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub itch: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub kobo: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub smashwords: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub typewriter: Option<String>,
}

impl SourceLinks {
    pub fn new() -> SourceLinks {
        SourceLinks {
            development: None,
            online: None,
            print: None,
            digital: None,
        }
    }
}
