use serde::{Deserialize, Serialize};

/// Describes the various categories of work based on their length.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub enum LengthCategory {
    // More than 50k words.
    #[serde(alias = "Novel", alias = "novel")]
    Novel,

    /// 20k to 50k words.
    #[serde(alias = "Novella", alias = "novella")]
    Novella,

    /// 10k to 20k words.
    #[serde(alias = "Novelette", alias = "novelette")]
    Novelette,

    /// 1k to 10k words.
    #[serde(alias = "Story", alias = "story")]
    Story,

    // Under 1k words.
    #[serde(alias = "Flash", alias = "flash")]
    Flash,
}
