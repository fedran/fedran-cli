use serde::{Deserialize, Serialize};

/// Gives the description of the project, used for creating the back title page and the summary.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct SourceDescription {
    /// Contains the tag line, typically written in bold.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub tag: Option<String>,

    /// Contains the rest of the paragraphs of the description.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub paragraphs: Option<Vec<String>>,
}
