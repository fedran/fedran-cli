use serde::{Deserialize, Serialize};

/// Indicates various states of the volume in respect to moving through the
/// status toward stable.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct SourceState {
    /// If the project is public, then the source files should be available.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub public_source: Option<bool>,

    /// If the project has at least one chapter written and checked into the
    /// repository. If this optional, then the state is undefined and probably
    /// should be treated as having chapters.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub started_chapters: Option<bool>,

    /// If all the chapters have been completed, then this will be true. This
    /// would also be considered "content complete".
    #[serde(skip_serializing_if = "Option::is_none")]
    pub finished_chapters: Option<bool>,

    /// All stories require a line or development edit.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub line_edited: Option<bool>,

    /// All stories require a copy edit cleanup.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub copy_edited: Option<bool>,

    /// If the project has been posted on a public forum, on the site,
    /// or somewhere else, then it has been posted. Typically, this
    /// would also need the source code available.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub available_online: Option<bool>,

    /// If the project has been published in print format, it is "published".
    #[serde(skip_serializing_if = "Option::is_none")]
    pub available_print: Option<bool>,

    /// If the project is done and edited, only maintenance is left, then
    /// it is "done done".
    #[serde(skip_serializing_if = "Option::is_none")]
    pub done: Option<bool>,
}
