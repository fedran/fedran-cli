use crate::config::get_config;
use crate::data::chapters::Chapter;
use crate::data::characters::Character;
use crate::data::sources::covers::SourceCover;
use crate::data::sources::credits::SourceCredits;
use crate::data::sources::description::SourceDescription;
use crate::data::sources::length::LengthCategory;
use crate::data::sources::state::SourceState;
use crate::data::sources::status::SourceStatus;
use crate::data::warnings::SourceWarning;
use anyhow::Result;
use links::SourceLinks;
use serde::{Deserialize, Serialize};
use slug::slugify;
use std::cell::RefCell;
use std::cmp::Ordering;
use std::fmt::{Display, Formatter};
use std::fs;
use std::rc::Rc;

pub mod covers;
pub mod credits;
pub mod description;
pub mod length;
pub mod links;
pub mod state;
pub mod status;

/// Contains information about a source, which would be a story, a novel, or an interactive game.
/// The data for a given source is documented and controlled by its Git repository with data then
/// pushed up to the `fedran-meta` project for dissemination to the rest of the system.
///
/// The key for a given project is `pov` and `volume`. It is typically formatted as `0000-00` with
/// `pov` being the first field.  The combined format is called a "source identifier".
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct Source {
    /// The POV that identifies the character for this source.
    pub pov: u16,

    /// The zero-based volume for a given POV.
    pub volume: u8,

    /// The title of the source.
    pub title: String,

    /// Contains the description of the project.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<SourceDescription>,

    /// Contains the total number of words for the entire source.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub word_count: Option<usize>,

    /// Contains the general length categorization.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub length: Option<LengthCategory>,

    /// The slug of the title.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub title_slug: Option<String>,

    /// The slug of the identifier and title.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub identifier_slug: Option<String>,

    /// The copyright year of the source.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub copyright_year: Option<u16>,

    /// The status of the identifier. If this is None, then it is considered Unstable.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<SourceStatus>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub phase: Option<i8>,

    /// An optional list of categories for this source.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub categories: Option<Vec<String>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub genres: Option<Vec<String>>,

    /// The individual states of the volume which are used to determine what
    /// linting is performed and what is considered a failure when a state is
    /// missing for a given status.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub state: Option<SourceState>,

    /// Includes information about the languages used in the source.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub languages: Option<SourceLanguages>,

    /// Includes a flag if the source has a dedication.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub has_dedication: Option<bool>,

    /// Contains information about the Git repository for this source.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub git: Option<SourceGit>,

    /// Various links to the volume, both in print and online.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub links: Option<SourceLinks>,

    /// Information about the cover.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cover: Option<SourceCover>,

    /// A merged list of all the warnings.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub warnings: Option<SourceWarning>,

    /// A list of credits for the work.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub credits: Option<SourceCredits>,

    /// Contains a sequence of all the chapter metadata.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub chapters: Option<Vec<Chapter>>,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct SourceGit {
    pub http_url: Option<String>,
    pub branch: Option<String>,
}

/// Describes the languages that are used in the source. This determines which pages are included
/// in the front of the generated files.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct SourceLanguages {
    /// If true, then the source uses Lorban names.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lorban: Option<bool>,

    /// If true, then the source uses Miwāfu names.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub miwafu: Option<bool>,

    /// If true, then the source uses Hissan names.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub hissan: Option<bool>,
}

/// Encapsulates the data used to identify a source including the unique characters in Fedran and
/// each of the sources written from their points of view. This is the code representation of
/// https://gitlab.com/fedran/fedran-meta/-/blob/master/sources.json
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct SourceData {
    /// The file versioning, which is an incrementing value and identifies structural changes to
    /// the file.
    version: u8,

    /// Contains the various characters in the data.
    pub characters: Vec<Rc<RefCell<Character>>>,

    /// Contains all of the known sources.
    pub sources: Vec<Rc<RefCell<Source>>>,
}

impl Source {
    /// Formats the entire identifier as a string in the format of "0000-00".
    pub fn format_identifier(&self) -> String {
        format!("{:04}-{:02}", self.pov, self.volume)
    }

    /// Formats the source identifier as a zero-padded string.
    pub fn format_volume(&self) -> String {
        format!("{:02}", self.volume)
    }

    /// Gets the normalized slug used for paths in the Fedran site for the
    /// character.
    pub fn get_slug(&self) -> String {
        slugify(self.title.as_str())
    }

    /// Retrieves the URL for the source on the Fedran site.
    pub fn get_http_url(&self) -> String {
        format!("https://fedran.com/{}/", self.get_slug())
    }

    /// Retrieves the URL for the source on the Fedran site.
    pub fn get_git_url(&self) -> String {
        format!(
            "https://src.mfgames.com/fedran-sources/{}/",
            self.get_slug()
        )
    }

    pub fn get_git_http_url(&self) -> String {
        format!(
            "https://src.mfgames.com/fedran-sources/{}.git",
            self.get_slug()
        )
    }
}

impl Display for Source {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{} {}", self.format_identifier(), self.title)
    }
}

impl SourceGit {
    pub fn new() -> SourceGit {
        SourceGit {
            http_url: None,
            branch: None,
        }
    }
}

impl SourceData {
    /// Retrieves the source from either a local directory or from Gitlab, parses the JSON into a
    /// typesafe collection of data, and then returns the result as something that can be
    /// manipulated or queried.
    pub async fn get() -> Result<SourceData> {
        // Figure out how we are downloading this.
        let config = get_config()?;
        let sources_file = config.sources_file;
        let content = match sources_file {
            None => {
                let target = "https://gitlab.com/fedran/fedran-meta/-/raw/master/sources.json";
                log::info!("downloading sources from {}", target);
                let response = reqwest::get(target).await?;
                response.text().await?
            }
            Some(sources_file) => {
                log::info!("loading sources from {:?}", sources_file);
                fs::read_to_string(&sources_file)?
            }
        };

        // Deserialize the results.
        let set = serde_json::from_str(&content)?;

        Ok(set)
    }

    /// Commits changes to the sources, as a commit on Gitlab or a direct write on disk.
    pub async fn commit(&self, message: String) -> Result<()> {
        // Report what we're doing.
        log::info!("updating sources -> {}", message);

        // Serialize ourselves into pretty JSON.
        let buf = Vec::new();
        let formatter = serde_json::ser::PrettyFormatter::with_indent(b"    ");
        let mut ser = serde_json::Serializer::with_formatter(buf, formatter);
        self.serialize(&mut ser).unwrap();

        // Write it out based on configuration.
        let config = get_config()?;
        let sources_file = config.sources_file.clone();

        match sources_file {
            None => {
                log::error!("cannot handle uploading to Gitlab");
                panic!("cannot handle GitLab");
            }
            Some(sources_file) => {
                log::info!("writing sources to {:?}", sources_file);
                fs::write(&sources_file, ser.into_inner())?;
            }
        };

        Ok(())
    }

    /// Retrieves a character based on their POV identifier. If the POV can't be found, this returns
    /// None.
    pub fn get_character_from_pov(&self, pov: u16) -> Option<Rc<RefCell<Character>>> {
        for character in &self.characters {
            if character.borrow().pov == pov {
                let character = Rc::clone(character);
                return Some(character);
            }
        }

        None
    }

    /// Retrieves a source based on the POV and volume. If it cannot be found, then this returns
    /// None.
    pub fn get_source_from_pov_and_volume(
        &self,
        pov: u16,
        volume: u8,
    ) -> Option<Rc<RefCell<Source>>> {
        for source in &self.sources {
            let source_test = Rc::clone(source);
            let source_test = source_test.borrow();

            if source_test.pov == pov && source_test.volume == volume {
                let source = Rc::clone(source);
                return Some(source);
            }
        }

        None
    }

    /// Replaces or adds a source in the list.
    pub fn set_source(mut self, source: Rc<RefCell<Source>>) -> SourceData {
        // Go through the sources and inject the new source if we find a matching one.
        let mut added = false;

        let sources = self
            .sources
            .into_iter()
            .map(|x| {
                match &x.borrow().pov == &source.borrow().pov
                    && &x.borrow().volume == &source.borrow().volume
                {
                    true => {
                        added = true;
                        Rc::clone(&source)
                    }
                    false => Rc::clone(&x),
                }
            })
            .collect();

        self.sources = sources;

        // If we didn't add it, then tack it in on the end since we'll be sorting.
        if !added {
            log::info!("adding source {}", &source.borrow());
            self.sources.push(source);
        }

        // Make sure everything is sorted.
        self.sources.sort_by(|a, b| {
            let pov_comparison = a.borrow().pov.cmp(&b.borrow().pov);

            match pov_comparison {
                Ordering::Equal => a.borrow().volume.cmp(&b.borrow().volume),
                __ => pov_comparison,
            }
        });

        self
    }
}
