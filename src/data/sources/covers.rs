use serde::{Deserialize, Serialize};

/// Information about the cover for the source.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct SourceCover {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cover_slug: Option<String>,

    /// A list of lines used to figure out the line breaks on the title page.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub title_page_lines: Option<Vec<String>>,
}
