use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub enum SourceStatus {
    #[serde(alias = "Stable", alias = "stable")]
    Stable,
    #[serde(alias = "Beta", alias = "beta")]
    Beta,
    #[serde(alias = "Alpha", alias = "alpha")]
    Alpha,
    #[serde(alias = "Unstable", alias = "unstable")]
    Unstable,
    #[serde(alias = "Idea", alias = "idea")]
    Idea,
}
