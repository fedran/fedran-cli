use serde::{Deserialize, Serialize};

/// Describes the various credits for the piece.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct SourceCredits {
    /// A list of the folk that have read the unedited version and have given feedback.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alpha_readers: Option<Vec<String>>,

    /// A list of folk that have read the edited version and have given feedback.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub beta_readers: Option<Vec<String>>,

    /// A list of sensitivity readers at any stage.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sensitivity_readers: Option<Vec<String>>,

    /// A list of editors used throughout the process.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub editors: Option<Vec<String>>,
}
