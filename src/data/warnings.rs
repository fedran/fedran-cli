use crate::config::get_config;
use crate::data::sources::length::LengthCategory;
use anyhow::Result;
use serde::{Deserialize, Serialize};
use std::cell::RefCell;
use std::fmt::{Display, Formatter};
use std::fs;
use std::rc::Rc;

/// Contains information about a single normalized warning used within Fedran.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct Warning {
    /// Contains the name of the warning.
    pub name: String,
}

/// Describes the standard set of warnings for chapter matter.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct WarningSet {
    /// Warnings that apply to the primary or main character.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub primary: Option<Vec<String>>,

    /// Warnings that apply to a secondary character present.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub secondary: Option<Vec<String>>,

    /// Warnings that referenced within the chapter but not shown "on page".
    #[serde(skip_serializing_if = "Option::is_none")]
    pub referenced: Option<Vec<String>>,
}

/// Includes the formatting information about a warning block for the entire source.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct SourceWarning {
    /// Contains the formatting block for the legal page.
    pub text: Option<String>,

    /// Warnings that apply to the primary or main character.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub primary: Option<Vec<String>>,

    /// Warnings that apply to a secondary character present.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub secondary: Option<Vec<String>>,

    /// Warnings that referenced within the chapter but not shown "on page".
    #[serde(skip_serializing_if = "Option::is_none")]
    pub referenced: Option<Vec<String>>,
}

/// Encapsulates the collection of warning from the metadata repository.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct WarningData {
    /// The file versioning, which is an incrementing value and identifies structural changes to
    /// the file.
    version: u8,

    /// Contains all the known warnings within the system.
    pub warnings: Vec<Rc<RefCell<Warning>>>,
}

impl Display for Warning {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{}", self.name)
    }
}

impl WarningSet {
    /// Combines two warning sets into a third one which has everything moved to the most
    /// significant category and all duplicates removed. It also sorts alphabetically each of the
    /// items regardless of initial order.
    pub fn union(&self, set: &WarningSet) -> WarningSet {
        // First combine the lists together.
        let primary = WarningSet::union_combine_lists(&self.primary, &set.primary);
        let mut secondary = WarningSet::union_combine_lists(&self.secondary, &set.secondary);
        let mut referenced = WarningSet::union_combine_lists(&self.referenced, &set.referenced);

        // Then move duplicates up so a referenced is overshadowed by a secondary of the same type.
        secondary = WarningSet::union_uplift_lists(&primary, &secondary);
        referenced = WarningSet::union_uplift_lists(&primary, &referenced);
        referenced = WarningSet::union_uplift_lists(&secondary, &referenced);

        // Finally, create the resulting list.
        WarningSet {
            primary,
            secondary,
            referenced,
        }
    }

    fn union_combine_lists(
        x: &Option<Vec<String>>,
        y: &Option<Vec<String>>,
    ) -> Option<Vec<String>> {
        match x {
            None => match y {
                None => None,
                Some(yv) => Some(yv.to_vec()),
            },
            Some(xv) => match y {
                None => Some(xv.to_vec()),
                Some(yv) => {
                    let mut combined = [xv.to_vec(), yv.to_vec()].concat();

                    combined.sort_unstable();
                    combined.dedup();

                    Some(combined)
                }
            },
        }
    }

    /// Removes items from the less significant list that are already in the more significant list.
    fn union_uplift_lists(
        more_significant: &Option<Vec<String>>,
        less_significant: &Option<Vec<String>>,
    ) -> Option<Vec<String>> {
        match more_significant {
            None => match &less_significant {
                None => None,
                Some(less) => Some(less.to_vec()),
            },
            Some(more) => match &less_significant {
                None => None,
                Some(less) => {
                    let mut less = less.to_vec();

                    for search in more {
                        less.retain(|a| a != search);
                    }

                    match less.len() {
                        0 => None,
                        _ => Some(less.to_vec()),
                    }
                }
            },
        }
    }
}

impl SourceWarning {
    pub fn from_sets(
        sets: Vec<&Option<WarningSet>>,
        length: &Option<LengthCategory>,
    ) -> SourceWarning {
        // Start by creating an empty set.
        let mut combined = WarningSet {
            primary: None,
            secondary: None,
            referenced: None,
        };

        // Combine all the warning sets from the list.
        for next in sets {
            if let Some(set) = next {
                combined = combined.union(&set);
            }
        }

        // The prefix is always: "This X contains" where X is the category.
        let mut first = true;
        let mut buffer = format!(
            "This {} contains",
            match length {
                None => "work",
                Some(l) => match l {
                    LengthCategory::Flash => "flash fiction",
                    LengthCategory::Story => "story",
                    LengthCategory::Novelette => "novelette",
                    LengthCategory::Novella => "novella",
                    LengthCategory::Novel => "novel",
                },
            }
        );

        // Primary warnings are phrased as: "scenes where the primary character experiences: X.".
        if let Some(items) = &combined.primary {
            buffer = format!(
                "{} scenes where the primary character and others experience {}.",
                buffer,
                SourceWarning::comma_and(items)
            );
            first = false;
        };

        // Secondary warnings.
        if let Some(items) = &combined.secondary {
            buffer = match first {
                true => format!(
                    "{} scenes where secondary characters experience {}.",
                    buffer,
                    SourceWarning::comma_and(items)
                ),
                false => format!(
                    "{} There are also scenes where secondary characters experience {}.",
                    buffer,
                    SourceWarning::comma_and(items)
                ),
            };
            first = false;
        };

        // References.
        if let Some(items) = &combined.referenced {
            buffer = match first {
                true => format!(
                    "{} off-page references {}.",
                    buffer,
                    SourceWarning::comma_and(items)
                ),
                false => format!(
                    "{} There are off-page references to {}.",
                    buffer,
                    SourceWarning::comma_and(items)
                ),
            };
            first = false;
        };

        // Add in the trailer.
        buffer = match first {
            true => format!("{} no scenes of sexual assault.", buffer),
            false => format!("{} There is no sexual assault.", buffer),
        };

        // Copy the results into the wrapped object.
        SourceWarning {
            text: Some(buffer),
            primary: combined.primary,
            secondary: combined.secondary,
            referenced: combined.referenced,
        }
    }

    fn comma_and(items: &Vec<String>) -> String {
        // We need to manipulate this list. Start with the simple conditions.
        let len = &items.len();
        let mut items: Vec<String> = items
            .to_vec()
            .iter()
            .map(|a| WarningData::format_lowercase(a.to_string()))
            .collect();

        items.sort_unstable();

        if len == &1usize {
            return items[0].to_owned();
        }

        if len == &2usize {
            return format!("{} and {}", items[0], items[1]);
        }

        // We use proper Oxford commas, so we need a ", and " with the last one.
        items[len - 1] = format!("and {}", items[len - 1]);
        items.join(", ")
    }
}

impl WarningData {
    /// Retrieves the warning from either a local directory or from Gitlab, parses the JSON into a
    /// typesafe collection of data, and then returns the result as something that can be
    /// manipulated or queried.
    pub async fn get() -> Result<WarningData> {
        // Figure out how we are downloading this.
        let config = get_config()?;
        let warnings_file = config.warnings_file;
        let content = match warnings_file {
            None => {
                let target = "https://src.mfgames.com/fedran-metadata/fedran-metadata/raw/branch/main/warnings.json";
                log::info!("downloading warnings from {}", target);
                let response = reqwest::get(target).await?;
                response.text().await?
            }
            Some(warnings_file) => {
                log::info!("loading warnings from {:?}", warnings_file);
                fs::read_to_string(&warnings_file)?
            }
        };

        // Deserialize the results.
        let set = serde_json::from_str(&content)?;

        Ok(set)
    }

    pub fn format_lowercase(input: String) -> String {
        input.to_lowercase()
    }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn set_union() {
        let set1 = WarningSet {
            primary: Some(vec!["Physical Assault".to_owned()]),
            secondary: None,
            referenced: None,
        };
        let set2 = WarningSet {
            primary: Some(vec!["Mental Assault".to_owned()]),
            secondary: None,
            referenced: None,
        };
        let result = set1.union(&set2);

        assert_eq!(
            result.primary,
            Some(vec![
                "Mental Assault".to_owned(),
                "Physical Assault".to_owned()
            ])
        );
        assert_eq!(result.secondary, None);
        assert_eq!(result.referenced, None);
    }

    #[test]
    fn set_union_remove_duplicates() {
        let set1 = WarningSet {
            primary: Some(vec!["Physical Assault".to_owned()]),
            secondary: None,
            referenced: None,
        };
        let set2 = WarningSet {
            primary: Some(vec!["Physical Assault".to_owned()]),
            secondary: None,
            referenced: None,
        };
        let result = set1.union(&set2);

        assert_eq!(result.primary, Some(vec!["Physical Assault".to_owned()]));
        assert_eq!(result.secondary, None);
        assert_eq!(result.referenced, None);
    }

    #[test]
    fn set_union_remove_uplifted() {
        let set1 = WarningSet {
            primary: Some(vec![
                "Physical Assault".to_owned(),
                "Mental Assault".to_owned(),
            ]),
            secondary: None,
            referenced: None,
        };
        let set2 = WarningSet {
            primary: None,
            secondary: Some(vec!["Physical Assault".to_owned()]),
            referenced: Some(vec!["Mental Assault".to_owned()]),
        };
        let result = set1.union(&set2);

        assert_eq!(
            result.primary,
            Some(vec![
                "Physical Assault".to_owned(),
                "Mental Assault".to_owned()
            ])
        );
        assert_eq!(result.secondary, None);
        assert_eq!(result.referenced, None);
    }

    #[test]
    fn format_primary_1() {
        let set = WarningSet {
            primary: Some(vec!["Physical Assault".to_owned()]),
            secondary: None,
            referenced: None,
        };
        let source = SourceWarning::from_sets(vec![&Some(set)], &Some(LengthCategory::Novel));

        assert_eq!("This novel contains scenes where the primary character and others experience physical assault. There is no sexual assault.", source.text.unwrap());
    }

    #[test]
    fn format_primary_2() {
        let set = WarningSet {
            primary: Some(vec![
                "Physical Assault".to_owned(),
                "Mental Assault".to_owned(),
            ]),
            secondary: None,
            referenced: None,
        };
        let source = SourceWarning::from_sets(vec![&Some(set)], &Some(LengthCategory::Novel));

        assert_eq!("This novel contains scenes where the primary character and others experience mental assault and physical assault. There is no sexual assault.", source.text.unwrap());
    }

    #[test]
    fn format_primary_3() {
        let set = WarningSet {
            primary: Some(vec![
                "Physical Assault".to_owned(),
                "Mental Assault".to_owned(),
                "Emotional Abuse".to_owned(),
            ]),
            secondary: None,
            referenced: None,
        };
        let source = SourceWarning::from_sets(vec![&Some(set)], &Some(LengthCategory::Novel));

        assert_eq!("This novel contains scenes where the primary character and others experience emotional abuse, mental assault, and physical assault. There is no sexual assault.", source.text.unwrap());
    }
}
