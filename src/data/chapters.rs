use crate::data::warnings::WarningSet;
use anyhow::Result;
use chrono::NaiveDate;
use serde::{Deserialize, Serialize};
use std::fs;
use std::path::PathBuf;
use yaml_front_matter::YamlFrontMatter;

/// Contains the common metadata at the top of every chapter.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct Chapter {
    /// The title of the chapter, which is always required.
    pub title: String,

    /// The number of words in the chapter.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub word_count: Option<usize>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub date: Option<NaiveDate>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub summary: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub teaser: Option<String>,

    /// Contains the optional warnings for the chapter.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub warnings: Option<WarningSet>,
}

impl Chapter {
    pub fn from_file(path: PathBuf) -> Result<Chapter> {
        // Report what we're doing.
        log::debug!("loading chapter from {:?}", path);

        // Split out the front matter from the content.
        let contents = fs::read_to_string(path)?;
        let contents = contents.as_str();
        let document = YamlFrontMatter::parse::<Chapter>(contents).unwrap();
        let mut chapter = document.metadata;

        // Figure out the word count for the chapter.
        let content = document.content;
        let counts = words_count::count(content);

        chapter.word_count = Some(counts.words);

        // Return the results.
        Ok(chapter)
    }
}
