use anyhow::Result;
use serde::{Deserialize, Serialize};
use slug::slugify;
use std::fmt::{Display, Formatter};

/// Contains information about a specific character including `pov` which is the unique key to
/// identify them across the entire system. Character data is considered "metadata" and is managed
/// by the `fedran-meta` project.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct Character {
    pub pov: u16,

    pub name: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub character_slug: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub color: Option<String>,
}

impl Character {
    /// Retrieves the zero-padded identifier for the character.
    pub fn format_pov(&self) -> String {
        format!("{:04}", self.pov)
    }

    /// Gets the normalized slug used for paths in the Fedran site for the
    /// character.
    pub fn get_slug(&self) -> String {
        slugify(self.name.as_str())
    }

    /// Retrieves the URL for the character on the Fedran site.
    pub fn get_url(&self) -> String {
        format!("https://fedran.com/{}/", self.get_slug())
    }
}

impl Display for Character {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{:04} {}", self.pov, self.name)
    }
}
