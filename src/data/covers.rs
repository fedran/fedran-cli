use crate::config::get_config;
use anyhow::Result;
use serde::{Deserialize, Serialize};
use std::cell::RefCell;
use std::fmt::{Display, Formatter};
use std::fs;
use std::rc::Rc;

/// Contains information about a cover. The data for this is located in the `fedran-covers`
/// repository.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct Cover {
    /// The POV that identifies the character for this source.
    pub pov: u16,

    /// The zero-based volume for a given POV.
    pub volume: u8,

    /// The slug of the title.
    pub title_slug: String,
}

/// Encapsulates the data used to list covers. This is stored in the Gitea server and we use a
/// custom endpoint to retrieve the results.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct CoverData {
    /// Contains the various characters in the data.
    pub covers: Vec<Rc<RefCell<Cover>>>,
}

impl Cover {
    /// Parses the standard name of the cover and create a cover entry out of it.
    pub fn new(name: String) -> Cover {
        let name = name.replace(".svg", "");
        let parts: Vec<&str> = name.splitn(3, "-").collect();

        if parts.len() != 3 {
            panic!("cannot parse cover file: {}", name);
        }

        Cover {
            pov: parts[0].parse::<u16>().unwrap(),
            volume: parts[1].parse::<u8>().unwrap(),
            title_slug: parts[2].to_string(),
        }
    }

    /// Formats the entire identifier as a string in the format of "0000-00".
    pub fn format_identifier(&self) -> String {
        format!("{:04}-{:02}", self.pov, self.volume)
    }

    pub fn get_cover_slug(&self) -> String {
        format!("{}-{}", self.format_identifier(), self.title_slug)
    }
}

impl Display for Cover {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{}", self.get_cover_slug())
    }
}

impl CoverData {
    /// Retrieves the source from either a local directory or from Gitlab, parses the JSON into a
    /// typesafe collection of data, and then returns the result as something that can be
    /// manipulated or queried.
    pub async fn get() -> Result<CoverData> {
        // Figure out how we are downloading this.
        let config = get_config()?;
        let covers_repo_dir = config.covers_repo_dir;
        let mut covers: Vec<Rc<RefCell<Cover>>> = vec![];

        match covers_repo_dir {
            None => {
                panic!("cannot handle covers_repo_dir not being defined");
            }
            Some(covers_path) => {
                log::info!("loading covers from {:?}", covers_path);

                let covers_path = covers_path.join("covers");
                let entries = fs::read_dir(covers_path)?;

                for entry in entries {
                    let entry = entry?;

                    if entry.path().is_file() {
                        let name = entry.file_name();
                        let name = name.into_string().unwrap();

                        if name.ends_with(".svg") && !name.contains("template") {
                            let cover = Cover::new(name);
                            let cover = RefCell::new(cover);
                            let cover = Rc::new(cover);

                            covers.push(cover);
                        }
                    }
                }
            }
        };

        // Create the cover and return it.
        Ok(CoverData { covers })
    }

    /// Retrieves a cover based on the POV and volume. If it cannot be found, then this returns
    /// None.
    pub fn get_from_pov_and_volume(&self, pov: u16, volume: u8) -> Option<Rc<RefCell<Cover>>> {
        for cover in &self.covers {
            let cover_test = Rc::clone(cover);
            let cover_test = cover_test.borrow();

            if cover_test.pov == pov && cover_test.volume == volume {
                let found = Rc::clone(cover);
                return Some(found);
            }
        }

        None
    }
}
