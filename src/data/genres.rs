use crate::config::get_config;
use anyhow::Result;
use serde::{Deserialize, Serialize};
use std::cell::RefCell;
use std::fmt::{Display, Formatter};
use std::fs;
use std::rc::Rc;

/// Contains information about a single normalized genre used within Fedran.
#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
pub struct Genre {
    /// Contains the name of the genre.
    pub name: String,

    /// Contains the summary of the genre.
    pub summary: String,
}

/// Encapsulates the collection of genre from the metadata repository.
#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct GenreData {
    /// The file versioning, which is an incrementing value and identifies structural changes to
    /// the file.
    version: u8,

    /// Contains all the known genres within the system.
    pub genres: Vec<Rc<RefCell<Genre>>>,
}

impl Display for Genre {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{}", self.name)
    }
}

impl GenreData {
    /// Retrieves the genre from either a local directory or from Gitlab, parses the JSON into a
    /// typesafe collection of data, and then returns the result as something that can be
    /// manipulated or queried.
    pub async fn get() -> Result<GenreData> {
        // Figure out how we are downloading this.
        let config = get_config()?;
        let genres_file = config.genres_file;
        let content = match genres_file {
            None => {
                let target = "https://gitlab.com/fedran/fedran-meta/-/raw/master/genres.json";
                log::info!("downloading genres from {}", target);
                let response = reqwest::get(target).await?;
                response.text().await?
            }
            Some(genres_file) => {
                log::info!("loading genres from {:?}", genres_file);
                fs::read_to_string(&genres_file)?
            }
        };

        // Deserialize the results.
        let set = serde_json::from_str(&content)?;

        Ok(set)
    }
}
